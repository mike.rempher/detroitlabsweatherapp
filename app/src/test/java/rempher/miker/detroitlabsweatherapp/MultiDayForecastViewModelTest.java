package rempher.miker.detroitlabsweatherapp;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import rempher.miker.detroitlabsweatherapp.model.City;
import rempher.miker.detroitlabsweatherapp.model.Forecast;
import rempher.miker.detroitlabsweatherapp.model.Weather;
import rempher.miker.detroitlabsweatherapp.ui.multidayforecast.MultiDayForecastViewModel;

public class MultiDayForecastViewModelTest {

    private MultiDayForecastViewModel multiDayForecastViewModel;

    @Before
    public void setup() {
        multiDayForecastViewModel = new MultiDayForecastViewModel();
    }

    @Test
    public void getFilteredList_sortingList() {
        City city = new City();
        city.setName("lansing");

        Forecast mForecast = new Forecast();
        mForecast.setCity(city);

        List<Weather> weatherList = new ArrayList<>();

        Weather weather1 = new Weather();
        weather1.setDate(1541131200);
        weatherList.add(weather1);

        Weather weather2 = new Weather();
        weather2.setDate(1541480400);
        weatherList.add(weather2);

        Weather weather3 = new Weather();
        weather3.setDate(1541480400);
        weatherList.add(weather3);

        Weather weather4 = new Weather();
        weather4.setDate(1541566800);
        weatherList.add(weather4);

        Weather weather5 = new Weather();
        weather5.setDate(1541653200);
        weatherList.add(weather5);

        Weather weather6 = new Weather();
        weather6.setDate(1541739600);
        weatherList.add(weather6);

        Weather weather7 = new Weather();
        weather7.setDate(1541304000);
        weatherList.add(weather7);

        mForecast.setWeatherList(weatherList);
        multiDayForecastViewModel.setForecast(mForecast);

        multiDayForecastViewModel.getFilteredList();

        Assert.assertEquals(1541131200, multiDayForecastViewModel.getFilteredList().get(0).getDate().getTime() / 1000);
        Assert.assertEquals(1541480400, multiDayForecastViewModel.getFilteredList().get(1).getDate().getTime() / 1000);
        Assert.assertEquals(1541566800, multiDayForecastViewModel.getFilteredList().get(2).getDate().getTime() / 1000);
        Assert.assertEquals(1541653200, multiDayForecastViewModel.getFilteredList().get(3).getDate().getTime() / 1000);
        Assert.assertEquals(1541304000, multiDayForecastViewModel.getFilteredList().get(4).getDate().getTime() / 1000);
    }
}
