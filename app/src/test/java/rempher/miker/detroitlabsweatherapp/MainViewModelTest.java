package rempher.miker.detroitlabsweatherapp;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import rempher.miker.detroitlabsweatherapp.model.Weather;
import rempher.miker.detroitlabsweatherapp.model.WeatherInfo;
import rempher.miker.detroitlabsweatherapp.model.WeatherType;
import rempher.miker.detroitlabsweatherapp.ui.main.MainViewModel;

public class MainViewModelTest {

    private MainViewModel mainViewModel;
    private String degreeSymbol = "°F";

    @Before
    public void setup() {
        mainViewModel = new MainViewModel();
    }

    @Test
    public void setWeather_null() {
        mainViewModel.setWeather(null);
    }

    @Test
    public void setWeather_isSunny() {
        WeatherInfo testWeatherInfoObject = new WeatherInfo();
        testWeatherInfoObject.setTemp(45.00);
        testWeatherInfoObject.setTempLow(32.00);
        testWeatherInfoObject.setTempHigh(50.00);

        WeatherType testWeatherTypeObject = new WeatherType();
        testWeatherTypeObject.setDescription("sunny");
        testWeatherTypeObject.setIconId("01d");

        List<WeatherType> weatherTypes = new ArrayList<>();
        weatherTypes.add(testWeatherTypeObject);

        Weather testWeatherObject = new Weather();
        testWeatherObject.setCityName("lansing");
        testWeatherObject.setInfo(testWeatherInfoObject);
        testWeatherObject.setTypes(weatherTypes);

        mainViewModel.setWeather(testWeatherObject);

        Assert.assertEquals("lansing", mainViewModel.city.get());
        Assert.assertEquals(45.00 + degreeSymbol, mainViewModel.temp.get());
        Assert.assertEquals(32.00 + degreeSymbol, mainViewModel.tempLow.get());
        Assert.assertEquals(50.00 + degreeSymbol, mainViewModel.tempHigh.get());
        Assert.assertEquals("sunny", mainViewModel.description.get());
        Assert.assertEquals(R.drawable.sunny, mainViewModel.image.get());
    }

    @Test
    public void setWeather_null_weatherTypes() {
        Weather testWeatherObject = new Weather();
        testWeatherObject.setTypes(null);

        mainViewModel.setWeather(testWeatherObject);

        Assert.assertEquals("N/A", mainViewModel.description.get());
    }

    @Test
    public void setWeather_null_weatherInfo() {
        Weather testWeatherObject = new Weather();
        testWeatherObject.setInfo(null);

        mainViewModel.setWeather(testWeatherObject);

        Assert.assertEquals("N/A", mainViewModel.temp.get());
        Assert.assertEquals("N/A", mainViewModel.tempLow.get());
        Assert.assertEquals("N/A", mainViewModel.tempHigh.get());
    }

    @Test
    public void setLocation_null_location() {
        mainViewModel.setLocation(null);
    }
}
