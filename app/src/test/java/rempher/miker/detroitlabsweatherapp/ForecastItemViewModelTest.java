package rempher.miker.detroitlabsweatherapp;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import rempher.miker.detroitlabsweatherapp.model.Weather;
import rempher.miker.detroitlabsweatherapp.model.WeatherInfo;
import rempher.miker.detroitlabsweatherapp.model.WeatherType;
import rempher.miker.detroitlabsweatherapp.ui.multidayforecast.ForecastItemViewModel;

public class ForecastItemViewModelTest {

    private ForecastItemViewModel fViewModel;
    private String degreeSymbol = "°F";

    @Test
    public void forecastItemViewModel_null_weather() {
        fViewModel = new ForecastItemViewModel(null);
    }

    @Test
    public void forecastItemViewModel_rainy() {
        WeatherInfo testWeatherInfoObject = new WeatherInfo();
        testWeatherInfoObject.setTemp(45.00);
        testWeatherInfoObject.setTempLow(32.00);
        testWeatherInfoObject.setTempHigh(50.00);

        WeatherType testWeatherTypeObject = new WeatherType();
        testWeatherTypeObject.setDescription("rainy");
        testWeatherTypeObject.setIconId("10d");
        List<WeatherType> weatherTypes = new ArrayList<>();
        weatherTypes.add(testWeatherTypeObject);

        Weather testWeatherObject = new Weather();
        testWeatherObject.setCityName("lansing");
        testWeatherObject.setDate(154113120);
        testWeatherObject.setInfo(testWeatherInfoObject);
        testWeatherObject.setTypes(weatherTypes);

        fViewModel = new ForecastItemViewModel(testWeatherObject);

        Assert.assertEquals(45.00 + degreeSymbol, fViewModel.temp.get());
        Assert.assertEquals(32.00 + degreeSymbol, fViewModel.tempLow.get());
        Assert.assertEquals(50.00 + degreeSymbol, fViewModel.tempHigh.get());
        Assert.assertEquals("Tuesday", fViewModel.date.get());
        Assert.assertEquals("rainy", fViewModel.description.get());
        Assert.assertEquals(R.drawable.rain, fViewModel.image.get());
    }
}
