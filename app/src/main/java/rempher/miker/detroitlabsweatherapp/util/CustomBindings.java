package rempher.miker.detroitlabsweatherapp.util;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

public class CustomBindings {
    @BindingAdapter("imageResource")
    public static void setImageViewResource(ImageView iv, int resId) {
        iv.setImageResource(resId);
    }
}
