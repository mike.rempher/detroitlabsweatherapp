package rempher.miker.detroitlabsweatherapp.model;

import com.google.gson.annotations.SerializedName;

public class WeatherType {

    @SerializedName("main")
    private String type;

    private String description;

    @SerializedName("icon")
    private String iconId;

    public String getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public String getIconId() {
        return iconId;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setIconId(String iconId) {
        this.iconId = iconId;
    }
}
