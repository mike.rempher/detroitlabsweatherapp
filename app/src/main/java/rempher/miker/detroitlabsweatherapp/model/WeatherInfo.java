package rempher.miker.detroitlabsweatherapp.model;

import com.google.gson.annotations.SerializedName;

public class WeatherInfo {

    private double temp;

    @SerializedName("temp_min")
    private double tempLow;

    @SerializedName("temp_max")
    private double tempHigh;

    public double getTemp() {
        return temp;
    }

    public double getTempLow() {
        return tempLow;
    }

    public double getTempHigh() {
        return tempHigh;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public void setTempLow(double tempLow) {
        this.tempLow = tempLow;
    }

    public void setTempHigh(double tempHigh) {
        this.tempHigh = tempHigh;
    }
}
