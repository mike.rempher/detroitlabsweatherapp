package rempher.miker.detroitlabsweatherapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class Weather {
    private int id;

    @SerializedName("name")
    private String cityName;

    @SerializedName("main")
    private WeatherInfo info;

    @SerializedName("weather")
    private List<WeatherType> types;

    @SerializedName("dt")
    private long dateSeconds;

    public int getId() {
        return id;
    }

    public WeatherInfo getInfo() {
        return info;
    }

    public List<WeatherType> getTypes() {
        return types;
    }

    public String getCityName() {
        return cityName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public void setInfo(WeatherInfo info) {
        this.info = info;
    }

    public void setTypes(List<WeatherType> types) {
        this.types = types;
    }


    public void setDate(long seconds) {
        this.dateSeconds = seconds;
    }

    public Date getDate() {
        return new Date(dateSeconds * 1000);
    }
}
