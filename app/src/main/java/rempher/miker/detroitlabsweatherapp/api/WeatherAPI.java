package rempher.miker.detroitlabsweatherapp.api;

import rempher.miker.detroitlabsweatherapp.ApiManager;
import rempher.miker.detroitlabsweatherapp.model.Forecast;
import rempher.miker.detroitlabsweatherapp.model.Weather;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherAPI {
    @GET(ApiManager.URL + "weather")
    Call<Weather> getWeather(@Query("lat") double lat, @Query("lon") double lon, @Query("units") String units, @Query("APPID") String appId);


    @GET(ApiManager.URL + "forecast")
    Call<Forecast> getForecast(@Query("lat") double lat, @Query("lon") double lon, @Query("units") String units, @Query("APPID") String appId);
}