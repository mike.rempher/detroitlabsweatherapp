package rempher.miker.detroitlabsweatherapp.ui.multidayforecast;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField;
import android.location.Location;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import rempher.miker.detroitlabsweatherapp.model.Forecast;
import rempher.miker.detroitlabsweatherapp.model.Weather;


public class MultiDayForecastViewModel extends ViewModel {

    public ObservableField<String> name = new ObservableField<>();

    private Forecast mForecast;
    private MutableLiveData<Location> mLocationLiveData = new MutableLiveData<>();

    void setLocation(Location location) {
        mLocationLiveData.postValue(location);
    }

    public void setForecast(Forecast forecast) {
        if (forecast != null) {
            mForecast = forecast;
            name.set(forecast.getCity().getName());
        }
    }

    public MutableLiveData<Location> getLocationLiveData() {
        return mLocationLiveData;
    }

    public List<Weather> getFilteredList() {
        ArrayList<String> daysAdded = new ArrayList<>();
        ArrayList<Weather> newList = new ArrayList<>();

        List<Weather> currentList = mForecast.getWeatherList();

        for (Weather item : currentList) {
            String day = getDayOfWeek(item.getDate());
            if (!daysAdded.contains(day)) {
                daysAdded.add(day);
                newList.add(item);

                if (newList.size() >= 5) {
                    break;
                }
            }
        }
        return newList;
    }


    private String getDayOfWeek(Date date) {
        return new SimpleDateFormat("EEEE", Locale.US).format(date);
    }
}
