package rempher.miker.detroitlabsweatherapp.ui.main;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import rempher.miker.detroitlabsweatherapp.ApiManager;
import rempher.miker.detroitlabsweatherapp.R;
import rempher.miker.detroitlabsweatherapp.databinding.ActivityMainBinding;
import rempher.miker.detroitlabsweatherapp.model.Weather;
import rempher.miker.detroitlabsweatherapp.ui.multidayforecast.MultiDayForecastActivity;
import rempher.miker.detroitlabsweatherapp.util.SimpleLocationListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final long LOCATION_REFRESH_TIME = 15;
    private static final float LOCATION_REFRESH_DISTANCE = 123456;
    private static final int LOCATION_REQUEST = 12;

    private MainViewModel mViewModel;
    private LocationManager mLocationManager;

    private final LocationListener mLocationListener = new SimpleLocationListener() {
        @Override
        public void onLocationChanged(final Location location) {

            mViewModel.setLocation(location);

            ApiManager.getInstance().getWeatherApi().getWeather(location.getLatitude(), location.getLongitude(), "imperial", getString(R.string.open_weather_api))
                    .enqueue(new Callback<Weather>() {
                        @Override
                        public void onResponse(@NonNull Call<Weather> call, @NonNull Response<Weather> response) {
                            mViewModel.setWeather(response.body());
                        }

                        @Override
                        public void onFailure(@NonNull Call<Weather> call, @NonNull Throwable t) {

                        }
                    });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        binding.setMainViewModel(mViewModel);

        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        requestLocationUpdates();

        Button navigateButton = findViewById(R.id.viewExtendedForecast);

        navigateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MultiDayForecastActivity.class);
                intent.putExtra("location", mViewModel.getLocation());
                startActivity(intent);
            }
        });
    }

    private void requestLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    LOCATION_REQUEST);

        } else {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_REFRESH_TIME,
                    LOCATION_REFRESH_DISTANCE, mLocationListener);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == LOCATION_REQUEST) {
                requestLocationUpdates();
            }
        } else {
            /**
             * in real world scenario would create alert dialog to
             * warn user location services are required for proper application function
             */
            finish();
        }
    }
}
