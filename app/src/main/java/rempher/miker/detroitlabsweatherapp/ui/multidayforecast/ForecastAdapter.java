package rempher.miker.detroitlabsweatherapp.ui.multidayforecast;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import rempher.miker.detroitlabsweatherapp.databinding.RecycleritemForecastBinding;
import rempher.miker.detroitlabsweatherapp.model.Weather;

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder> {

    private List<Weather> mWeatherList;

    public ForecastAdapter(List<Weather> weatherList) {
        mWeatherList = weatherList;
    }

    @NonNull
    @Override
    public ForecastViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        RecycleritemForecastBinding binding = RecycleritemForecastBinding.inflate(inflater, viewGroup, false);
        return new ForecastViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ForecastViewHolder forecastViewHolder, int position) {
        Weather weather = mWeatherList.get(position);
        ForecastItemViewModel forecastItemViewModel = new ForecastItemViewModel(weather);
        forecastViewHolder.bind(forecastItemViewModel);
    }

    @Override
    public int getItemCount() {
        return mWeatherList.size();
    }

    class ForecastViewHolder extends RecyclerView.ViewHolder {
        private final RecycleritemForecastBinding binding;

        ForecastViewHolder(RecycleritemForecastBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(ForecastItemViewModel forecastItemViewModel) {
            binding.setForecastVieModel(forecastItemViewModel);
            binding.executePendingBindings();
        }
    }
}




