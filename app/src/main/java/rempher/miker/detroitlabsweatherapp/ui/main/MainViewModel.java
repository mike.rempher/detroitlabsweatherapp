package rempher.miker.detroitlabsweatherapp.ui.main;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.location.Location;

import java.util.List;

import rempher.miker.detroitlabsweatherapp.R;
import rempher.miker.detroitlabsweatherapp.model.Weather;
import rempher.miker.detroitlabsweatherapp.model.WeatherInfo;
import rempher.miker.detroitlabsweatherapp.model.WeatherType;

public class MainViewModel extends ViewModel {

    private String degreeSymbol = "°F";
    private Location mLocation;

    public ObservableInt image = new ObservableInt();
    public ObservableField<String> city = new ObservableField<>();
    public ObservableField<String> temp = new ObservableField<>();
    public ObservableField<String> description = new ObservableField<>();
    public ObservableField<String> tempLow = new ObservableField<>();
    public ObservableField<String> tempHigh = new ObservableField<>();

    private void setIconImage(String iconId) {
        switch (iconId) {
            case "01d":
            case "01n":
                image.set(R.drawable.sunny);
                break;
            case "02d":
            case "02n":
                image.set(R.drawable.few_clouds);
                break;
            case "03d":
            case "03n":
                image.set(R.drawable.cloudy);
                break;
            case "04d":
            case "04n":
                image.set(R.drawable.few_clouds);
                break;
            case "09d":
            case "09n":
                image.set(R.drawable.heavy_rain);
                break;
            case "10d":
            case "10n":
                image.set(R.drawable.rain);
                break;
            case "11d":
            case "11n":
                image.set(R.drawable.thunderstorm);
                break;
            case "13d":
            case "13n":
                image.set(R.drawable.snow);
                break;
            case "50d":
            case "50n":
                image.set(R.drawable.mist);
                break;
        }

    }

    public void setWeather(Weather weather) {
        if (weather == null) {
            return;
        }

        city.set(String.valueOf(weather.getCityName()));

        List<WeatherType> types = weather.getTypes();
        if (types != null && types.size() != 0) {
            setIconImage(types.get(0).getIconId());
            description.set(String.valueOf(types.get(0).getDescription()));
        } else {
            description.set("N/A");
        }

        WeatherInfo info = weather.getInfo();
        if (info != null) {
            temp.set(String.valueOf(info.getTemp()) + degreeSymbol);
            tempLow.set(String.valueOf(info.getTempLow()) + degreeSymbol);
            tempHigh.set(String.valueOf(info.getTempHigh()) + degreeSymbol);
        } else {
            temp.set("N/A");
            tempLow.set("N/A");
            tempHigh.set("N/A");
        }
    }

    public void setLocation(Location location) {
        if (location != null) {
            mLocation = location;
        }
    }

    public Location getLocation() {
        return mLocation;
    }
}
