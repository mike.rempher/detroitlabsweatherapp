package rempher.miker.detroitlabsweatherapp.ui.multidayforecast;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import java.util.Objects;

import rempher.miker.detroitlabsweatherapp.ApiManager;
import rempher.miker.detroitlabsweatherapp.R;
import rempher.miker.detroitlabsweatherapp.databinding.ActivityMultidayforecastBinding;
import rempher.miker.detroitlabsweatherapp.model.Forecast;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MultiDayForecastActivity extends AppCompatActivity {

    private MultiDayForecastViewModel mViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMultidayforecastBinding binding = DataBindingUtil.setContentView(
                this, R.layout.activity_multidayforecast);

        mViewModel = ViewModelProviders.of(this).get(MultiDayForecastViewModel.class);
        binding.setMultiDayViewModel(mViewModel);

        subscribeUI();

        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Location location = getIntent().getParcelableExtra("location");
        mViewModel.setLocation(location);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void subscribeUI() {
        mViewModel.getLocationLiveData().observe(this, new android.arch.lifecycle.Observer<Location>() {
            @Override
            public void onChanged(@Nullable Location location) {
                if (location != null) {
                    loadForecast(location);
                }
            }
        });
    }

    private void loadForecast(Location location) {
        ApiManager.getInstance().getWeatherApi().getForecast(location.getLatitude(), location.getLongitude(), "imperial", getString(R.string.open_weather_api))
                .enqueue(new Callback<Forecast>() {
                    @Override
                    public void onResponse(@NonNull Call<Forecast> call, @NonNull Response<Forecast> response) {

                        mViewModel.setForecast(response.body());

                        RecyclerView rv = findViewById(R.id.forecastRecyclerView);
                        ForecastAdapter adapter = new ForecastAdapter(mViewModel.getFilteredList());


                        rv.setLayoutManager(new LinearLayoutManager(MultiDayForecastActivity.this));
                        rv.setAdapter(adapter);
                    }

                    @Override
                    public void onFailure(@NonNull Call<Forecast> call, @NonNull Throwable t) {

                    }
                });
    }

}
