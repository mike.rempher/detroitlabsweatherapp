package rempher.miker.detroitlabsweatherapp;

import com.google.gson.Gson;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import rempher.miker.detroitlabsweatherapp.api.WeatherAPI;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiManager {
    private static final ApiManager instance = new ApiManager();

    public static final String URL = "http://api.openweathermap.org/data/2.5/";

    private Retrofit mRetrofit;

    public static ApiManager getInstance() {
        return instance;
    }

    private ApiManager() {
        if (mRetrofit == null) {
            mRetrofit = createRetroFitClient(createHttpClient());
        }
    }

    private Retrofit createRetroFitClient(OkHttpClient client) {
        Retrofit.Builder builder = new Retrofit.Builder();
        return builder.baseUrl(URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();
    }

    private OkHttpClient createHttpClient() {
        return new OkHttpClient.Builder()
                .retryOnConnectionFailure(true)
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
                .build();
    }

    public WeatherAPI getWeatherApi() {
        return mRetrofit.create(WeatherAPI.class);
    }
}
